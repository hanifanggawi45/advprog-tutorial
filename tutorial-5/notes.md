# Requirements

1. Log Entity

    Log yang dimiliki tiap mahasiswa yang menjadi asisten. Berelasi One to Many dengan entity Mahasiswa
    Class yang dibutuhkan :
    
    - Log (model)
    - LogController (RestController)
    - LogRepositroy (JpaRepository)
    - LogService & LogServiceImpl (Service)
    - class Report (Class tambahan untuk menampilkan report dari kumpulan log)

2. Relasi antar Mahasiswa dan MataKuliah
    
    Relasi Mahasiswa mendaftar ke suatu MataKuliah adalah one to many. yang perlu dibuat:
    - Field daftarMahasiswa pada model MataKuliah (Model)
    - Field MataKuliah pada pada model Mahasiswa (Model)
    - endpoint untuk mendaftarkan mahasiswa menjadi asisten suatu mata kuliah (RestController)
    - service yang menghandle logic dari endpoint tersebut (Service)

3. Relasi antar Mahasiswa dan Log
    
    Relasi Mahasiswa meyimpan Log adalah one to many. yang perlu dibuat:
    - Field daftarLog pada model Mahasiswa (Model)
    - Field Mahasiswa pada pada model Log (Model)
    - endpoint untuk menambahkan log seorang mahasiswa (RestController)
    - service yang menghandle logic dari endpoint tersebut serta menggabungkan log menjadi Report (Service)