package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Report;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LogServiceImpl implements  LogService{

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Log createLog(Log log, Mahasiswa mahasiswa) {
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        mahasiswa.addLog(log);
        mahasiswaRepository.save(mahasiswa);

        return log;
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log getLogById(Integer idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public Log updateLog(Integer idLog, Log log, Mahasiswa mahasiswa) {
        log.setIdLog(idLog);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        mahasiswa.addLog(log);
        mahasiswaRepository.save(mahasiswa);
        return log;
    }

    @Override
    public void deleteLogbyId(Integer idLog) {
        logRepository.deleteById(idLog);
    }

    public List<Report> createReport(Mahasiswa mahasiswa) {
        List<Report> reports = new ArrayList<>();
        List<Log> logs = mahasiswa.getDaftarLog();
        Map<String, List<Log>> monthlyLogs = new HashMap<String, List<Log>>();
        for (Log log : logs) {
            String key = log.getStart().getMonth().toString();
            if (!monthlyLogs.containsKey(key)) {
                List<Log> monthlyLog = new ArrayList<Log>();
                monthlyLog.add(log);
                monthlyLogs.put(key, monthlyLog);
            } else {
                monthlyLogs.get(key).add(log);
            }
        }

        for (String month : monthlyLogs.keySet()) {
            int jamKerja = 0;
            for (Log log : monthlyLogs.get(month)) {
                jamKerja += log.getDuration();
            }
            Report report = new Report(month, jamKerja, 350*jamKerja );
            reports.add(report);
        }
        return reports;
    }
}
