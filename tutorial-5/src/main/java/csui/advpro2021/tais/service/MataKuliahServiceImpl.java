package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MataKuliahServiceImpl implements MataKuliahService {
    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public Iterable<MataKuliah> getListMataKuliah() {
        return mataKuliahRepository.findAll();
    }

    @Override
    public MataKuliah getMataKuliah(String kodeMatkul) {
        return mataKuliahRepository.findByKodeMatkul(kodeMatkul);
    }

    @Override
    public MataKuliah createMataKuliah(MataKuliah mataKuliah) {
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public MataKuliah updateMataKuliah(String kodeMatkul, MataKuliah mataKuliah) {
        mataKuliah.setKodeMatkul(kodeMatkul);
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public MataKuliah daftarAsisten(MataKuliah mataKuliah, Mahasiswa mahasiswa) {
//        MataKuliah mataKuliah = mataKuliahRepository.findByKodeMatkul(kodeMatkul);
//        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);

        mataKuliah.daftarAsisten(mahasiswa);
        mahasiswa.setMataKuliah(mataKuliah);
        mahasiswaRepository.save(mahasiswa);
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public void deleteMataKuliah(String kodeMatkul) {
        MataKuliah matkul = this.getMataKuliah(kodeMatkul);
        mataKuliahRepository.delete(matkul);
    }

}
