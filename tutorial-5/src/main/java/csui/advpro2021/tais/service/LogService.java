package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Report;

import java.util.List;

public interface LogService {
    Log createLog(Log log, Mahasiswa mahasiswa);

    Iterable<Log> getListLog();

    Log getLogById(Integer logId);

    Log updateLog(Integer idLog, Log log, Mahasiswa mahasiswa);

    List<Report> createReport(Mahasiswa mahasiswa);

    void deleteLogbyId(Integer idLog);
}
