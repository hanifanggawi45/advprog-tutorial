package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Report;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LogController {

    @Autowired
    private LogService logService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @PostMapping(path = "/mahasiswa/{npm}/addlog",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity addLog(@RequestBody Log log, @PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.createLog(log, mahasiswa));
    }

    @PutMapping(path = "/log/{idLog}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@RequestBody Log log, @PathVariable(value = "idLog") Integer idLog) {
        Mahasiswa mahasiswa = log.getMahasiswa();
        return ResponseEntity.ok(logService.updateLog(idLog, log , mahasiswa));
    }

    @DeleteMapping(path = "/log/{idLog}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity deleteLog(@RequestBody Log log, @PathVariable(value = "idLog") Integer idLog) {
        logService.deleteLogbyId(idLog);
        return ResponseEntity.ok(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "mahasiswa/{npm}/reports", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getReports(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.createReport(mahasiswa));
    }

}
