package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@NoArgsConstructor
@Data
@Table(name="log")
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_log", updatable = false, nullable = false)
    private Integer idLog;

    @Column(name = "start", nullable = false)
    private LocalDateTime start;

    @Column(name = "finish", nullable = false)
    private LocalDateTime finish;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
//    @JoinColumn(name = "id_mahasiswa")
    @JsonBackReference
    private Mahasiswa mahasiswa;

    public Log(Integer idLog, LocalDateTime start, LocalDateTime finish, String deskripsi) {
        this.idLog = idLog;
        this.start = start;
        this.finish = finish;
        this.deskripsi = deskripsi;
    }

    public int getDuration() {
        long hours = ChronoUnit.HOURS.between(finish, start);
        return (int) hours;
    }

}
