package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mahasiswa")
@Data
@NoArgsConstructor
public class Mahasiswa {

    @Id
    @Column(name = "npm", updatable = false, nullable = false)
    private String npm;

    @Column(name = "nama")
    private String nama;

    @Column(name = "email")
    private String email;

    @Column(name = "ipk")
    private String ipk;

    @Column(name = "no_telp")
    private String noTelp;

    @OneToMany(targetEntity = Log.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "log_fk", referencedColumnName = "npm")
//    @OneToMany(mappedBy = "mahasiswa")
    @JsonManagedReference
    private List<Log> daftarLog = new ArrayList<>();

    @ManyToOne
    @JsonBackReference
    private MataKuliah mataKuliah;

    public Mahasiswa(String npm, String nama, String email, String ipk, String noTelp) {
        this.npm = npm;
        this.nama = nama;
        this.email = email;
        this.ipk = ipk;
        this.noTelp = noTelp;
    }

    public void addLog(Log log) {
        this.daftarLog.add(log);
    }
}
