package csui.advpro2021.tais.model;

import lombok.Data;

@Data
public class Report {
    private String month;
    private int jamKerja;
    private int pembayaran;

    public Report(String month, int jamKerja, int pembayaran) {
        this.month = month;
        this.jamKerja = jamKerja;
        this.pembayaran = pembayaran;
    }

}
