package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mata_kuliah")
@Data
@NoArgsConstructor
public class MataKuliah {
    @Id
    @Column(name = "kode_matkul", updatable = false)
    private String kodeMatkul;

    @Column(name = "nama_matkul")
    private String nama;

    @Column(name = "prodi")
    private String prodi;

    @OneToMany(targetEntity = Mahasiswa.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "matakuliah_fk", referencedColumnName = "kode_matkul")
    @JsonManagedReference
    private List<Mahasiswa> daftarMahasiswa = new ArrayList<>();

    public MataKuliah(String kodeMatkul, String nama, String prodi) {
        this.kodeMatkul = kodeMatkul;
        this.nama = nama;
        this.prodi = prodi;
    }

    public void daftarAsisten(Mahasiswa mahasiswa) {
        this.daftarMahasiswa.add(mahasiswa);
    }
}
