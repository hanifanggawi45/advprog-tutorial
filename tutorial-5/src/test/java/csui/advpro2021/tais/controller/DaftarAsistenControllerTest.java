package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MahasiswaController.class)
public class DaftarAsistenControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;

    private MataKuliah matkul;

    @BeforeEach
    public void setUp() throws Exception{

        matkul = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        mvc.perform(post("/mata-kuliah")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(matkul)));

        mvc.perform(post("/mahasiswa")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(mahasiswa)));

        mvc.perform(post("/mata-kuliah/ADVPROG/daftar-asisten/1906192052")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(mahasiswa)));

        Log log1 = new Log(11, LocalDateTime.parse("2021-04-06T10:15:30"), LocalDateTime.parse("2021-04-06T10:16:30"), "log 1");

        mvc.perform(post("/mahasiswa/1906192052/addlog")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(log1)));

//        mvc.perform(get("/mahasiswa").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk()).andExpect(content()
//                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$[0].npm").value("1906192052"));
//


    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerHasilSetup() throws Exception {
//        assertEquals(matkul.getDaftarMahasiswa(), mahasiswaService.getListMahasiswa());
        System.out.println(mahasiswaService.getListMahasiswa());
//        assertEquals(matkul.getDaftarMahasiswa().get(0), mahasiswa);
    }

    @Test
    void testDurationTest() {
//        LocalDateTime st = LocalDateTime.of(2021, 12, 16, 7, 45, 55);
//        LocalDateTime fn = LocalDateTime.of(, 12, 16, 7, 45, 55);
//        LocalDateTime test = LocalDateTime.parse("2015-08-04T10:11:30");
        Log log = new Log(11, LocalDateTime.parse("2021-04-06T11:00:00"), LocalDateTime.parse("2021-04-06T10:00:00"), "log 1");
        System.out.println("=============================================IYAK==DISINIIIII==============");
        System.out.println(log.getStart());
        System.out.println(log.getFinish());
        assertEquals(1, log.getDuration());
    }
}
