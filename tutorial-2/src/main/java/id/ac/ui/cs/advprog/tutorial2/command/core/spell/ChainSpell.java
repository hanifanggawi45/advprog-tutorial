package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChainSpell implements Spell {
    List<Spell> spells;
    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }


    @Override
    public void cast() {
        for (Spell spell : spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i= spells.size() -1; i > -1; i--) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
