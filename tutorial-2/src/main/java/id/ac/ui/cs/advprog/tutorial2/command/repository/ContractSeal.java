package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    Spell latestSpell;

    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        Spell spell = spells.get(spellName);
        latestSpell = spell;
        spell.cast();
    }

    public void undoSpell() {
        latestSpell.undo();
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
