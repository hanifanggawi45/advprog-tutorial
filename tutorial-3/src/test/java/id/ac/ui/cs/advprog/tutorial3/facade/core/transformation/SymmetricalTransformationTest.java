package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SymmetricalTransformationTest {
    private Class<?> symmetricalClass;

    @BeforeEach
    public void setup() throws Exception {
        symmetricalClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.SymmetricalTransformation");
    }

    @Test
    public void testSymmetricalHasEncodeMethod() throws Exception {
        Method translate = symmetricalClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSymmetricalEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";

        Spell result = new SymmetricalTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testSymmetricalHasDecodeMethod() throws Exception {
        Method translate = symmetricalClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSymmetricalDecodesCorrectly() throws Exception {
        String text = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new SymmetricalTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

}
