package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class SymmetricalTransformation {
    private AbyssalTransformation abyssalTransformation;

    public SymmetricalTransformation() {
        this.abyssalTransformation = new AbyssalTransformation(13);
    }

    public Spell encode(Spell spell) {
        return process(spell);
    }

    public Spell decode(Spell spell) {
        return process(spell);
    }

    public Spell process(Spell spell) {
        // rot 13
        String text = spell.getText();
        Codex codex = spell.getCodex();
        char[] res = new char[text.length()];
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if       (c >= 'a' && c <= 'm') c += 13;
            else if  (c >= 'A' && c <= 'M') c += 13;
            else if  (c >= 'n' && c <= 'z') c -= 13;
            else if  (c >= 'N' && c <= 'Z') c -= 13;
            res[i] = c;
        }

        return new Spell(new String(res), codex);
    }
}
