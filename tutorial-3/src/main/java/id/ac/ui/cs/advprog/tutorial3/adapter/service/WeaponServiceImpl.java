package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.*;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private BowRepository bowRepository;


    @Override
    public List<Weapon> findAll() {
        for (Spellbook spellbook : spellbookRepository.findAll()) {
            Weapon adaptedSpellbook = new SpellbookAdapter(spellbook);
            weaponRepository.add(adaptedSpellbook);
        }
        for (Bow bow : bowRepository.findAll()) {
            Weapon adaptedBow = new BowAdapter(bow);
            weaponRepository.add(adaptedBow);
        }
        return weaponRepository.findAll();
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        String holder = weapon.getHolderName();
        String attackTypeName = (attackType == 0) ? "normal attack": "charged attack";
        String attackMessage = (attackType == 0) ? weapon.normalAttack() : weapon.chargedAttack();
        weaponRepository.save(weapon);
        String newLog = String.format("%s attacked with %s (%s): %s",holder, weaponName, attackTypeName, attackMessage);
        logRepository.addLog(newLog);
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
