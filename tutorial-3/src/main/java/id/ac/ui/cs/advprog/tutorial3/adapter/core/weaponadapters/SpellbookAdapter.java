package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean charging;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.charging = false;
    }

    @Override
    public String normalAttack() {
        this.charging = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        String message = (charging) ? "Magic power not enough for large spell" : spellbook.largeSpell();
        this.charging = !charging;
        return message;
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
