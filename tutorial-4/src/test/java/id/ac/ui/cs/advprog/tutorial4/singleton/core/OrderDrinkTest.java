package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class OrderDrinkTest {
    private OrderDrink orderDrink = OrderDrink.getInstance();
    private Class<?> orderDrinkClass;

    @BeforeEach
    public void setup() throws Exception {
        orderDrinkClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
    }

    @Test
    public void testOrderDrinkHasOnlyOneInstance() throws Exception {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertTrue(orderDrink1 == orderDrink2);
    }

    @Test
    public void testOrderDrinkHasGetInstanceMethod() throws Exception {
        Method getInstance = orderDrinkClass.getDeclaredMethod("getInstance");
        int methodModifiers = getInstance.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkHasSetDrinkMethod() throws Exception {
        Method setDrink = orderDrinkClass.getDeclaredMethod("setDrink", String.class);
        int methodModifiers = setDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkHasGetDrinkMethod() throws Exception {
        Method getDrink = orderDrinkClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkHasToStringMethod() throws Exception {
        Method toString = orderDrinkClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

}
