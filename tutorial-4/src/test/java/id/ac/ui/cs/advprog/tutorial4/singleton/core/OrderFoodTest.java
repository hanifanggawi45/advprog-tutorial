package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class OrderFoodTest {
    private OrderFood orderFood = OrderFood.getInstance();
    private Class<?> orderFoodClass;

    @BeforeEach
    public void setup() throws Exception {
        orderFoodClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
    }

    @Test
    public void testOrderFoodHasOnlyOneInstance() throws Exception {
        OrderFood orderFood1 = OrderFood.getInstance();
        OrderFood orderFood2 = OrderFood.getInstance();
        assertSame(orderFood1,orderFood2);
    }

    @Test
    public void testOrderFoodHasGetInstanceMethod() throws Exception {
        Method getInstance = orderFoodClass.getDeclaredMethod("getInstance");
        int methodModifiers = getInstance.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodHasSetFoodMethod() throws Exception {
        Method setFood = orderFoodClass.getDeclaredMethod("setFood", String.class);
        int methodModifiers = setFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodHasGetFoodMethod() throws Exception {
        Method getFood = orderFoodClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodHasToStringMethod() throws Exception {
        Method toString = orderFoodClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

}
