package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @InjectMocks
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderADrink() throws Exception {
        orderService.orderADrink("A Drink");
        assertEquals(orderService.getDrink().getDrink(), "A Drink");
    }

    @Test
    public void testOrderAFood() throws Exception {
        orderService.orderAFood("A Food");
        assertEquals(orderService.getFood().getFood(), "A Food");
    }

}
