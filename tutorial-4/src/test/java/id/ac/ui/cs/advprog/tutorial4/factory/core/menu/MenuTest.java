package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factories.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MenuTest {
    private Class<?> menuClass;
    private Menu menu;

    @Test
    public void testCreateInuzumaRamenMenu() throws Exception {
        IngredientFactory ramenFactory = new InuzumaRamenFactory();
        //Ingridients:
        //Noodle: Ramen
        //Meat: Pork
        //Topping: Boiled Egg
        //Flavor: Spicy
        menu = new InuzumaRamen("ramen");
        assertEquals(menu.getName(),"ramen");
        assertEquals(menu.getFlavor().getDescription(),ramenFactory.createFlavor().getDescription());
        assertEquals(menu.getMeat().getDescription(),ramenFactory.createMeat().getDescription());
        assertEquals(menu.getNoodle().getDescription(),ramenFactory.createNoodle().getDescription());
        assertEquals(menu.getTopping().getDescription(),ramenFactory.createTopping().getDescription());
        // assertEquals(menu.getMeat().getDescription(),"Adding Tian Xu Pork Meat...");
        // assertEquals(menu.getNoodle().getDescription(),"Adding Inuzuma Ramen Noodles...");
        // assertEquals(menu.getTopping().getDescription(),"Adding Guahuan Boiled Egg Topping");
    }

    @Test
    public void testCreateLiyuanSobaMenu() throws Exception {
        IngredientFactory ingredientFactory = new LiyuanSobaFactory();
        menu = new LiyuanSoba("soba");
        assertEquals(menu.getName(),"soba");
        assertEquals(menu.getFlavor().getDescription(),ingredientFactory.createFlavor().getDescription());
        assertEquals(menu.getMeat().getDescription(),ingredientFactory.createMeat().getDescription());
        assertEquals(menu.getNoodle().getDescription(),ingredientFactory.createNoodle().getDescription());
        assertEquals(menu.getTopping().getDescription(),ingredientFactory.createTopping().getDescription());
    }

    @Test
    public void testCreateMondoUdonMenu() throws Exception {
        IngredientFactory ingredientFactory = new MondoUdonFactory();
        menu = new MondoUdon("udon");
        assertEquals(menu.getName(),"udon");
        assertEquals(menu.getFlavor().getDescription(),ingredientFactory.createFlavor().getDescription());
        assertEquals(menu.getMeat().getDescription(),ingredientFactory.createMeat().getDescription());
        assertEquals(menu.getNoodle().getDescription(),ingredientFactory.createNoodle().getDescription());
        assertEquals(menu.getTopping().getDescription(),ingredientFactory.createTopping().getDescription());
    }

    @Test
    public void testCreateSnevnezhaShiratakiMenu() throws Exception {
        IngredientFactory ingredientFactory = new SnevnezhaShiratakiFactory();
        menu = new SnevnezhaShirataki("shirataki");
        assertEquals(menu.getName(),"shirataki");
        assertEquals(menu.getFlavor().getDescription(),ingredientFactory.createFlavor().getDescription());
        assertEquals(menu.getMeat().getDescription(),ingredientFactory.createMeat().getDescription());
        assertEquals(menu.getNoodle().getDescription(),ingredientFactory.createNoodle().getDescription());
        assertEquals(menu.getTopping().getDescription(),ingredientFactory.createTopping().getDescription());
    }




}
