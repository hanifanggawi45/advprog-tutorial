package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setUp() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuService.getClass().getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGetMenusReturn4Objects() throws Exception {
        assertEquals(menuService.getMenus().size(), 4);
    }
}
