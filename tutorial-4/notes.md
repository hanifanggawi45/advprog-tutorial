## Perbedaan *Lazy instantiation* dengan *Eager Instantiation* pada SIngleton

1. *Lazy Instantiation*

    Pada singleton yang menerapkan *Lazy Instantiation*, instance dari singletonnya hanya akan dibuat ketika dibutuhkan 
    saja, Yaitu ketika method getInstance() dipanggil. Keuntungan dari *Lazy approach* adalah menghemat memory karena 
    instancenya dibuat hanya ketika diperlukan. Kekurangan dari *Lazy approach* adalah pemanggilan getInstance() yang 
    pertama akan memakan waktu lebih lama karena instance baru dibuat pertama kali. 

2. *Eager Instantiation*

    Pada singleton yang menerapkan *Eager Instantiation*, instance dari singletonnya akan dibuat ketika classnya diload.
    Jadi instancenya sudah siap ketika method getInstance() dipanggil. Keuntungan dari *eager Approach* adalah pemanggilan
    instance lebih cepat dibandingkan dengan *lazy approach* karena instance tidak perlu dibuat lagi. Kekurangan dari 
    *eager approach* adalah program memerlukan memori lebih banyak walaupun instance dari singleton belum digunakan.